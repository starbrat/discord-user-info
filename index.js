const Discord = require("discord.js");
const client = new Discord.Client();
var express = require('express');
var app = express();

client.on("ready", () => { 
	console.log(`Running...`);
	app.get('/', function (req, res) {
		var data = {
			error : "No id specified"
		};
		if (req.query.id) {
			var user = client.users.get(req.query.id);
			if (user) {
				data = {
					username : user.username,
					discriminator : user.discriminator,
					avatar : user.avatar,
					avatarURL : user.avatarURL
				};
			}
			else{
				data = {
					error : "Invalid id or user not available"
				};
			}
		}
		res.send(data);
	});
	app.get('/jsonp/', function (req, res) {
		var data = {
			error : "No id specified"
		};
		if (req.query.id) {
			var user = client.users.get(req.query.id);
			if (user) {
				data = {
					username : user.username,
					discriminator : user.discriminator,
					avatar : user.avatar,
					avatarURL : user.avatarURL
				};
			}
			else{
				data = {
					error : "Invalid id or user not available"
				};
			}
		}
		res.jsonp(data);
	});
});

var port = process.env.PORT || 3000;
var host = '0.0.0.0';
var server = app.listen(port, host, function(){
	console.log("App started...");
});
client.login("bot_access_token");
