# README #

This is a Discord.js bot that returns user data in JSON when it is passed a GET parameter.

Start the bot and navigate to localhost:3000/?id=user_id to get user data. Discord bots can only get information from users that they share a server with.

Note: When hosting on Heroku, make sure to specify host as '0.0.0.0', otherwise your bot may crash.